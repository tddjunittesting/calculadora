/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traductorsimple;
import java.util.HashMap;

/**
 *
 * @author alumne
 */
class Traductor {

    private HashMap<String,String> diccionario = new HashMap<String,String>(); 
    
    void anyadirPalabra(String esp_palabra, String ing_palabra) {
        this.diccionario.put(esp_palabra, ing_palabra);
    }

    String traducirPalabra(String esp_palabra){
        return this.diccionario.get(esp_palabra);
    }

  
    
}
